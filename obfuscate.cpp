#include <string>

#include "obfuscate.h"

using namespace std;

string decode(const string& input)
{
  // choose a power of two => then compiler can replace "modulo x" by much faster "and (x-1)"
  // at least as long as passwordLength, can be longer, too ...
  // non-random password that looks like a string emitted by the runtime library: the more obvious, the less obvious. And vice versa ;-)
  static const char password[] = "main";
  const size_t passwordLength = sizeof(password);
  // out = in XOR NOT(password)
  string result = input;
  for (size_t i = 0; i < input.length(); i++)
    result[i] ^= ~password[i % passwordLength];
  return result;
}
