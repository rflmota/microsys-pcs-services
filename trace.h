#ifndef TRACE_
#define TRACE_


extern "C"
{
    __attribute__((no_instrument_function)) void __cyg_profile_func_enter(void *, void *);

    __attribute__((no_instrument_function)) void __cyg_profile_func_exit(void *, void *);

    __attribute__((no_instrument_function, constructor)) void trace_begin(void);
}

#endif