# MakeFile For LkM Interface
#arm-brcm-linux-gnueabi-g++ -c -o obj/MicrosysCloudServices.o MicrosysCloudServices.cpp -static -I/home/paxdev/sdk/platforms/paxngfp_201205/include -I./build/include -L./build/lib -L/home/paxdev/sdk/platforms/paxngfp_201205/lib -lcurla -lbrestclient-cppa -lssl -lcrypto -lz -lpthread -ldl -lrt

TOOLCHAIN_PATH=/home/paxdev/sdk/platforms/paxngfp_201205/
TOOLCHAIN_INC=$(TOOLCHAIN_PATH)include
TOOLCHAIN_LIB=$(TOOLCHAIN_PATH)lib

CROSS_COMPILE=arm-brcm-linux-gnueabi-
COMPILER=g++
ARCHIVE=ar
ARCHIVE_OPTIONS=rcs
OPTIONS= -static
DEBUGOPTIONS= -DDEBUG
WARNS=-W -Wextra -Wall -Wformat=2 -Werror
INCUSED=-I$(TOOLCHAIN_INC) -I./build/include
LIBSUSED=-L$(TOOLCHAIN_LIB) -L./build/lib_static -l:libPetroLog.a -lrestclient-cpp -lcurl -lssl -lcrypto -lz -lpthread -ldl -lrt
OBJDIR=./obj
LIBDIR=./lib
EXTRAOPT=-I$(TOOLCHAIN_INC) -L$(TOOLCHAIN_LIB) -I/home/paxdev/PAXDEV_WORK/source/LuaMagicInterface -L/home/paxdev/PAXDEV_WORK/source/LuaMagicInterface/bin -L/home/paxdev/PAXDEV_WORK/source/petrolog -I/home/paxdev/PAXDEV_WORK/source/petrolog -I./ -L./ -losal -Wl,-rpath=//opt/lib -Wl,-rpath=./lib -Wl,-rpath-link,"/home/paxdev/sdk/platforms/paxngfp_201205/lib"

ALL: clean LPROD LDEBUG Testeur
	@echo "===============  DONE   ================="
LPROD : LIBPROD STATICLIBPROD 
LDEBUG : LIBDEBUG STATICLIBDEBUG Testeur

LIBDEBUG: 
	@echo "=========================================="
	@echo "COMPILING MICROSYS PCS OBJECT DEBUG"
	@echo "=========================================="
	$(CROSS_COMPILE)$(COMPILER) -g -c -O0 -o $(OBJDIR)/libMicrosysCloudServices.o MicrosysCloudServices.cpp $(OPTIONS) $(DEBUGOPTIONS) $(INCUSED)
	# $(COMPILER)  -g -finstrument-functions -c -O0 -o $(OBJDIR)/libMicrosysCloudServices.o MicrosysCloudServices.cpp $(OPTIONS) $(DEBUGOPTIONS) $(INCUSED)

LIBPROD:
	@echo "=========================================="
	@echo "COMPILING MICROSYS PCS OBJECT PROD"
	@echo "=========================================="
	$(CROSS_COMPILE)$(COMPILER) -g -c -o $(OBJDIR)/cipher.o cipher.cc $(OPTIONS) $(INCUSED) $(LIBSUSED)
	$(CROSS_COMPILE)$(COMPILER) -g -c -o $(OBJDIR)/obfuscate.o obfuscate.cpp $(OPTIONS) $(INCUSED) $(LIBSUSED)
	$(CROSS_COMPILE)$(COMPILER) -g -c -O0 -o $(OBJDIR)/libMicrosysCloudServices.o MicrosysCloudServices.cpp $(OPTIONS) $(DEBUGOPTIONS) $(INCUSED)

STATICLIBDEBUG:
	@echo "=========================================="
	@echo "COMPILING MICROSYS PCS LIBRARY"
	@echo "=========================================="
	$(CROSS_COMPILE)$(ARCHIVE) $(ARCHIVE_OPTIONS) $(LIBDIR)/libMicrosysPCS_services_q.a $(OBJDIR)/*.o

STATICLIBPROD:
	@echo "=========================================="
	@echo "COMPILING MICROSYS PCS LIBRARY"
	@echo "=========================================="
	$(CROSS_COMPILE)$(ARCHIVE) $(ARCHIVE_OPTIONS) $(LIBDIR)/libMicrosysPCS_services.a $(OBJDIR)/*.o

Testeur:
	@echo "=========================================="
	@echo "COMPILING Testeur da shit"
	@echo "=========================================="
	$(CROSS_COMPILE)g++ -g -O0 main.c -I./ $(EXTRAOPT) ./lib/libMicrosysPCS_services_q.a  -L./build/lib -lmagicluainterface -losal -lts -lpng  -lz -lfreetype -lssl -lcrypto  -lm -lpthread -lxui $(INCUSED) $(LIBSUSED)  -o Testeur
	#gcc -g -O0 main.c -I./ ./lib/libMicrosysPCS_services_q.a  ./build/Local_lib_static/libPetroLog.a -L./build/lib -lmagicluainterface $(INCUSED) $(LIBSUSED)  -o Testeur

clean:
	rm -f ./lib/*.a ./obj/*.o  Testeur



