#include "trace.h"
#include <petrolog.h>

__attribute__ ((no_instrument_function)) void __cyg_profile_func_exit(void *func, void *caller) { 

    printf("€");
}

__attribute__ ((no_instrument_function)) void __cyg_profile_func_enter(void *func, void *caller)
{
	__PTRACE_DBG__("############# %p CALLED <- %p	#############", __func__, func, caller);
}
void trace_begin(void)

{
	petrolog_init(PETROLOG_ALL);
}