The configuration file `pcs` needs to be encrypted with AES-256-CBC with a passphrase, you can use **openssl**:

```
openssl enc -e -a -md sha1 -aes-256-cbc -pass pass:'PetroMaster' -in conf.txt -out cloud_services.conf
```

The passphrase is *hardcoded* and *obfuscated* in code:

```
const char STRX[] = "\xc2\xfb\xe2\xe3\x90\xdf\xff\xe5\xe5\x9a\xe0";
```