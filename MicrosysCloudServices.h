#ifndef STATIC_LIBRARY_H_MicrosysCloudServices
#define STATIC_LIBRARY_H_MicrosysCloudServices

#define PCS_R_OK  				 0
#define PCS_R_TIMEOUT  			-1
#define PCS_R_BUSY  			-2
#define PCS_R_INVALID_DATA      -3
#define PCS_R_COMM_ERROR        -4
#define PCS_R_CONF_FILE_ERROR   -5
#define PCS_R_CONF_NA_ERROR     -6
#define PCS_R_CONF_EMPTY_ERROR  -7
#define PCS_R_CONF_PARSE_ERROR  -8
#define PCS_R_RES_PARSE_ERROR   -9
#define PCS_R_RES_EMPTY        -10
#define PCS_R_MEM_ERROR        -11

#define C_INV_CONFIG  			-1000
#define C_NO_ENTRY  			-1001

#define MSYS_PCS_VERSION_MAJOR 0
#define MSYS_PCS_VERSION_MINOR 1
#define MSYS_PCS_VERSION_PATCH 0

#ifdef __cplusplus
extern "C"
{
#warning "Extern  C  activated"
#endif

    typedef enum
    {
    JOB_ERROR = -2000,
    JOB_INTERNAL_IGNORE = -1000,
    JOB_OPEN = 0,
    JOB_CLOSE = 1,
    JOB_CLOSE_OPEN = 2,
    JOB_UPDATE_CONF = 3
} JOB_TYPE;

    typedef enum
    {
        AWAITING_EXECUTION,
        EXECUTED_WITH_SUCCESS,
        ERROR_EXECUTING
    } OPERATION_STATUS;

typedef struct
{
    JOB_TYPE job_type;
    char uuid[36];
    unsigned short polling_interval;
} PCS_JOB_RESPONSE;

unsigned long msys_pcs_get_version(void);
int msys_pcs_init(unsigned short *pcs_polling_interval);
int msys_pcs_test_communication();
int msys_pcs_send_file(const char posID[8], const char* file_content, unsigned short size_content);
int msys_pcs_get_last_job(const char posID[8], PCS_JOB_RESPONSE *data);
// int msys_get_config(PCS_CONFIG_RESPONSE *data);

#ifdef __cplusplus
};
#endif

#endif