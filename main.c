#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "MicrosysCloudServices.h"
#include "petrolog.h"
#include <unistd.h>
#include "LuaMagicInterface/exportfunctions.h"

int main()
{
	// petrolog_init(PETROLOG_ALL);
	// printf("\n\n\nSTARTING GPRS SETUP AND CONNECTION . . .\n\n\n");
	// MagicInitGPRS("spe.inetd3.gdsp","a","a","");
	// printf("\n\n\nPLEASE WAIT . . .\n\n\n");
	// sleep(60);

	unsigned short interval;
	int res = msys_pcs_init(&interval);


	printf("\n\n\nService configured with polling interval of %d seconds . . .\n\n\n", interval);

	int tmp = msys_pcs_test_communication();

	if (tmp != PCS_R_OK)
	{
		printf("FAIL TO GET TEST COMM JOB :( !");
		// return 1;
	}

	PCS_JOB_RESPONSE data;

	int i = 0;

	while (1)
	{
		
		int res;
		try
		{
			res = msys_pcs_get_last_job("987654321", &data);
			/* code */
		}
		catch (...)
		{
			printf("MERDA");
		}
		if (res == PCS_R_OK || res == PCS_R_RES_EMPTY)
		{
			printf("GET LAST JOB OK!\n");
			printf("JOB UUID: %s\n", data.uuid);
			printf("JOB TYPE: %d\n", data.job_type);
		}
		else
		{
			__PTRACE_ERR__("ERROR COLLECTING LAST JOB :(");
			printf("TIMES %d", i);
			strncpy(data.uuid, "2806455c-c5b3-4c33-ade1-80d8fba9c9a7", 36);
			// return -1;
		}

		if (data.job_type == JOB_CLOSE_OPEN)
		{

			char str_data[512] = {'\0'};

			sprintf(str_data, "D00%.36s1%8d20180628170531039003653+00000001000101COMPRA    0010000000100VISA DB N 001+0000000100+00000000", data.uuid, 987654321);

			res = msys_pcs_send_file("987654321", str_data, sizeof(str_data));

			if (res == PCS_R_OK)
			{
				printf("SEND TOTALIZER OK!\n");
			}
			else
			{
				__PTRACE_ERR__("ERROR SENDING TOTALIZER :(");
				printf("TIMES %d", i);
				// return -1;
			}
			data.job_type = JOB_ERROR;
		}

		sleep(2);
		i++;
	}
	return PCS_R_OK;
}