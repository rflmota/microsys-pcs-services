#include "MicrosysCloudServices.h"

#include <string.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <streambuf>

// #include <restclient-cpp/connection.h>
#include <osal.h>
#include "connection.h"
#include <restclient-cpp/restclient.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/prettywriter.h>

#include <curl/curl.h>

#pragma region
#include <errno.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <fcntl.h>
#pragma endregion

#include "obfuscate.h"
#include "cipher.h"

using namespace std;
using namespace rapidjson;

//INTERNAL STRUCTS
struct Endpoint
{
	string url;
	unsigned int port;
};

const char STRX[] = "\xc2\xfb\xe2\xe3\x90\xdf\xff\xe5\xe5\x9a\xe0";

Endpoint endpoint;
int connectionTimeout = 0;
RestClient::Connection *connection = NULL;

unsigned long msys_pcs_get_version(void)
{
	return MSYS_PCS_VERSION_MAJOR * 0x10000 |
		   MSYS_PCS_VERSION_MINOR * 0x00100 |
		   MSYS_PCS_VERSION_PATCH * 0x00001;
}

int read_conf_file(string &conf_data)
{

	ifstream ifs("res/cloud_services.conf", ios::in | ios::binary);
	if (!ifs)
	{
		OsLog(LOG_ERROR, "ERROR OPENING CONFIGURATION FILE . . .");
		return PCS_R_CONF_FILE_ERROR;
	}

	// Descrypt the ciphertext
	Cipher mgr;
	string pw = decode(STRX);
	try
	{
		mgr.decrypt_file("res/cloud_services.conf", "res/cloud_services.conf", pw);
	}
	catch (...)
	{
		OsLog( LOG_ERROR, "<!> WARNING : CONFIGURATION FILE HAS ALREADY BEEN DECRYPTED AND IT'S IN PLAINSIGHT <!>");
	}

	conf_data = string(istreambuf_iterator<char>(ifs), istreambuf_iterator<char>());
	ifs.close();
	if (conf_data.empty())
	{
		OsLog( LOG_ERROR, "ERROR CONFIGURATION FILE EMPTY . . .");
		return PCS_R_CONF_FILE_ERROR;
	}

	return PCS_R_OK;
}

int parse_conf(string conf_data, unsigned short *pcs_polling_interval)
{
	rapidjson::Document newConf, persistentConf;

	string persistentConfData;
	read_conf_file(persistentConfData);

	if (newConf.Parse(conf_data.c_str()).HasParseError() || persistentConf.Parse(persistentConfData.c_str()).HasParseError())
	{
		OsLog( LOG_ERROR, "ERROR CONFIGURATION FILE EMPTY.");
		return PCS_R_CONF_PARSE_ERROR;
	}

	if (!newConf.IsObject() || !persistentConf.IsObject())
	{
		OsLog( LOG_ERROR, "ERROR CONFIGURATION FILE EMPTY.");
		return PCS_R_CONF_PARSE_ERROR;
	}

	rapidjson::Value::MemberIterator newJsonValue, persistentJsonValue;

	/*	for(Value::ConstMemberIterator itr = persistentConf.MemberBegin(); itr != persistentConf.MemberEnd(); ++itr) {
		__PTRACE_DBG__("JSON MEMBER : %s", itr->name.GetString());
		Value::MemberIterator testJsonValue = newConf.FindMember("url");
		itr->value.Swap(newJsonValue->value);
		itr->value = Value(newJsonValue->value, persistentConf.GetAllocator());
		itr->value.Set()
		// try {
		// 	itr = testJsonValue;
		// } catch (...) {}
	}

	StringBuffer sb;
    PrettyWriter<StringBuffer> PrettyWriter(sb);
    persistentConf.Accept(writer);    // Accept() traverses the DOM and generates Handler events.
    __PTRACE_INF__("\n\n%s\n\n", sb.GetString());*/

	newJsonValue = newConf.FindMember("url");
	if (newJsonValue == newConf.MemberEnd() || !newJsonValue->value.IsString())
	{
		OsLog( LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA . . .");
		// printf("ERROR CONFIGURATION FILE INVALID DATA . . .");
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
		endpoint.url = newJsonValue->value.GetString();

	newJsonValue = newConf.FindMember("port");
	if (newJsonValue == newConf.MemberEnd() || !newJsonValue->value.IsInt())
	{
		OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA . . .");
		// printf("ERROR CONFIGURATION FILE INVALID DATA . . .");
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
		endpoint.port = newJsonValue->value.GetInt();

	stringstream strEndpoint;
	strEndpoint << endpoint.url + ":" << endpoint.port;

	if (connection != NULL)
		delete connection;
	connection = new (std::nothrow) RestClient::Connection(strEndpoint.str());
	if (!connection)
	{
		OsLog(LOG_ERROR, "<!> ERROR <!> COULDN'T ALLOCATE MEMORY FOR CONNECTION OBJECT...");
		return PCS_R_MEM_ERROR;
	}

	RestClient::HeaderFields header;

	newJsonValue = newConf.FindMember("api-key");
	if (newJsonValue == newConf.MemberEnd() || !newJsonValue->value.IsString())
	{
		OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA [api-key] . . .");
		// printf("ERROR CONFIGURATION FILE INVALID DATA [api-key] . . .");
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
		header["api-key"] = newJsonValue->value.GetString();

	header["Content-Type"] = "application/json";

	header["accept"] = "application/json";

	connection->SetHeaders(header);

	newJsonValue = newConf.FindMember("timeout");
	if (newJsonValue == newConf.MemberEnd() || !newJsonValue->value.IsInt())
	{
		OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA [timeout]. . .");
		// printf("ERROR CONFIGURATION FILE INVALID DATA [timeout]. . .");
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
		connection->SetTimeout(newJsonValue->value.GetInt());

	newJsonValue = newConf.FindMember("polling");
	if (newJsonValue == newConf.MemberEnd() || !newJsonValue->value.IsInt())
	{
		OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA [polling]. . .");
		// printf("ERROR CONFIGURATION FILE INVALID DATA [polling]. . .");
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
		*pcs_polling_interval = newJsonValue->value.GetInt();

	return PCS_R_OK;
}

int msys_pcs_init(unsigned short *services_polling_interval)
{
	int op_res;
	
	OsLog(LOG_INFO,"# # #   MICROSYS PCS SERVICES   # # #");
	OsLog(LOG_INFO, "# # #	%s | %s	  # # #", __DATE__, __TIME__);
	OsLog(LOG_INFO, "# # #   %s   # # #", curl_version());
	OsLog(LOG_INFO, "#############	MSYS_PCS_INIT CALLED	#############");

	string conf_data;
	if ((op_res = read_conf_file(conf_data)) != PCS_R_OK)
		return op_res;

	OsLog(LOG_INFO, "INITIALIZING RestClient");

	int start_res = RestClient::init();
	if (start_res != 0)
	{
		OsLog(LOG_INFO, "CANNOT ENABLE RESTCLIENT...");
		return PCS_R_MEM_ERROR;
	}

	if ((op_res = parse_conf(conf_data, services_polling_interval)) != PCS_R_OK)
		return op_res;

	OsLog(LOG_INFO, "CREATING CONNECTION TO PCS ON : %s:%d", endpoint.url.c_str(), endpoint.port);
	OsLog(LOG_INFO, "=============	MSYS_PCS_INIT RETURNED	=============\n\n\n\n");
	return PCS_R_OK;
}

JOB_TYPE get_job_id_from_frame(const string &job)
{
	if (job == "PERIOD_OPEN")
	{
		return JOB_OPEN;
	}

	if (job == "PERIOD_CLOSE")
	{
		return JOB_CLOSE;
	}

	if (job == "PERIOD_CLOSE_THEN_OPEN")
	{
		return JOB_CLOSE_OPEN;
	}

	if (job == "UPDATE_CONFIGURATION")
	{
		return JOB_UPDATE_CONF;
	}

	// TODO : there's more OP types to implement

	return JOB_ERROR;
}

int msys_pcs_test_communication()
{
	OsLog(LOG_INFO, "#############	MSYS_PCS_TEST_COMM CALLED	#############\n\n\n\n");
	// printf("#############	MSYS_PCS_TEST_COMM CALLED	#############\n\n\n\n");

	if ((curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYPEER, 0) != CURLE_OK) || (curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYHOST, 0) != CURLE_OK))
	{
		OsLog(LOG_ERROR, "COULDN'T SETUP SECURE CONNECTION !");
		OsLog(LOG_ERROR,"=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n");
		return PCS_R_COMM_ERROR;
	}
	RestClient::Response response = connection->get("/api/v1/public/communication");
	OsLog(LOG_INFO, "## HTTP CODE : %d", response.code);

	if (response.code < 400)
	{
		if (response.code == 28)
		{
			OsLog(LOG_ERROR, "CONNECTION TIMED OUT !");
			OsLog(LOG_ERROR, "=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n\n");
			return PCS_R_TIMEOUT;
		}
		else if (response.code >= 400 || response.code < 0)
		{
			OsLog(LOG_ERROR, "PROBLEM WITH CONNECTION   ::   'LIBCURL' ERROR CODE # %d ", response.code);
			if (!response.body.empty())
			{
				OsLog(LOG_ERROR, "BODY >> %s", response.body.c_str());
			}
			return PCS_R_COMM_ERROR;
		}
	}
	OsLog(LOG_INFO, "RESULT: %s", string(response.body).c_str());
	OsLog(LOG_INFO,"=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n\n");
	return PCS_R_OK;
}

const char *get_status_frame_from_id(OPERATION_STATUS opID)
{
	switch (opID)
	{
	case AWAITING_EXECUTION:
		return "AWAITING_EXECUTION";
	case EXECUTED_WITH_SUCCESS:
		return "EXECUTED_WITH_SUCCESS";
	case ERROR_EXECUTING:
		return "ERROR_EXECUTING";
	}
}

int pcs_operation_send_feedback(const char posID[8], const char opID[36], OPERATION_STATUS opRes)
{
	stringstream serv;

	serv << "/api/v1/paymentTerminals/" << string(posID, 8) << "/operations/" << string(opID, 36) << "/status";

	rapidjson::StringBuffer strBuf;
	rapidjson::Writer<rapidjson::StringBuffer> serializer;
	strBuf.Clear();
	serializer.Reset(strBuf);
	serializer.StartObject();
	serializer.Key("message");
	serializer.String("");
	serializer.Key("terminalStatus");
	serializer.String(get_status_frame_from_id(opRes));
	serializer.Key("timestamp");
	char buf[24] = {0};
	time_t now = time(&now);

	struct tm *ptm = gmtime(&now);

	strftime(buf, 24, "%Y-%m-%dT%H:%M:%S.000Z", ptm);
	serializer.String(buf);
	serializer.EndObject();

	OsLog(LOG_INFO,"%s", strBuf.GetString());

	if ((curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYPEER, 0) != CURLE_OK) || (curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYHOST, 0) != CURLE_OK))
	{
		// printf("COULDN'T SETUP SECURE CONNECTION !");
		// printf("=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n");
		OsLog(LOG_ERROR,"COULDN'T SETUP SECURE CONNECTION !");
		OsLog(LOG_ERROR,"=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n");
		return PCS_R_COMM_ERROR;
	}
	RestClient::Response response = connection->post(serv.str(), strBuf.GetString());

	if (response.code != 200)
	{
		if (response.code == 201)
		{
			return PCS_R_OK;
		}
		else if (response.code == 28)
		{
			OsLog(LOG_ERROR, "CONNECTION TIMED OUT !");
			return PCS_R_TIMEOUT;
		}
		else if (response.code >= 400 || response.code < 0)
		{
			OsLog(LOG_ERROR, "PROBLEM WITH CONNECTION   ::   'LIBCURL' ERROR CODE # %d  - %s", response.code, !response.body.empty() ? response.body.c_str() : "EMPTY");
			return PCS_R_COMM_ERROR;
		}
	}

	return PCS_R_OK;
}

int msys_pcs_get_last_job(const char posID[8], PCS_JOB_RESPONSE *data)
{
	OsLog(LOG_INFO, "#############	MSYS_PCS_GET_LAST_JOB CALLED	#############");
	stringstream serv;
	std::string posIdMorph = string(posID, 8);

	serv << "/api/v1/paymentTerminals/" << posIdMorph << "/pendingOperations/first";

	OsLog(LOG_INFO,  "COLLECTION LAST JOB.... FOR POSID: %s", string(posIdMorph).c_str());
	// printf("COLLECTION LAST JOB.... FOR POSID: %s", string(posIdMorph).c_str());

	if ((curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYPEER, 0) != CURLE_OK) || (curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYHOST, 0) != CURLE_OK))
	{
		OsLog(LOG_ERROR, "COULDN'T SETUP SECURE CONNECTION !");
		OsLog(LOG_ERROR, "=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n");
		// printf("COULDN'T SETUP SECURE CONNECTION !");
		// printf("=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n");
		return PCS_R_COMM_ERROR;
	}

	RestClient::Response response = connection->get(serv.str());

	OsLog(LOG_INFO, "## HTTP CODE : %d", response.code);

	if (response.code != 200)
	{
		if (response.code == 204)
		{
			OsLog(LOG_ERROR, "NO LEFT PENDING OPERATIONS TO DO . . . ALL DONE !");
			OsLog(LOG_ERROR, "=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
			return PCS_R_RES_EMPTY;
		}
		else if (response.code == 28)
		{
			OsLog(LOG_ERROR,"CONNECTION TIMED OUT !");
			OsLog(LOG_ERROR,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
			return PCS_R_TIMEOUT;
		}
		else if (response.code >= 400 || response.code < 0)
		{
			OsLog(LOG_ERROR,"PROBLEM WITH CONNECTION   ::   'LIBCURL' ERROR CODE #%d  -  %s", response.code, response.body.c_str());
			OsLog(LOG_ERROR,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
			return PCS_R_COMM_ERROR;
		}
	}
	
	if (response.body.empty())
	{
		OsLog(LOG_ERROR,"NO RESPONSE!? BODY IS EMPTY!?");
		return PCS_R_RES_EMPTY;
	}
	
	OsLog(LOG_INFO, "BODY-DATA >> %s", response.body.c_str());
	rapidjson::Document jsonDOM;

	if ((jsonDOM.Parse(response.body.c_str()).HasParseError()))
	{
		OsLog(LOG_ERROR,"ERROR PARSING RESPONSE");
		OsLog(LOG_ERROR,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
		pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
		return PCS_R_RES_PARSE_ERROR;
	}
	if (!jsonDOM.IsObject())
	{
		OsLog(LOG_ERROR,"ERROR PARSING CONFIGURATION FILE . . .");
		OsLog(LOG_ERROR,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
		pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
		return PCS_R_CONF_PARSE_ERROR;
	}

	OsLog(LOG_INFO, "DATA : %s", response.body.c_str());

	rapidjson::Value::MemberIterator jsonValue;

	jsonValue = jsonDOM.FindMember("operationIdentifier");
	if (jsonValue == jsonDOM.MemberEnd() || !jsonValue->value.IsString() || jsonValue->value.GetStringLength() != 36)
	{
		OsLog(LOG_ERROR,"ERROR CONFIGURATION FILE INVALID DATA . . .");
		OsLog(LOG_ERROR,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
		pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
	{
		strncpy(data->uuid, jsonValue->value.GetString(), 36);
		std::string opId = jsonValue->value.GetString();
		OsLog(LOG_INFO, "UUID FOR THE JOB: %s", opId.c_str());
	}

	jsonValue = jsonDOM.FindMember("operationType");
	if (jsonValue == jsonDOM.MemberEnd() || !jsonValue->value.IsString())
	{
		OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA . . .");
		OsLog(LOG_ERROR, "=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
		// printf("ERROR CONFIGURATION FILE INVALID DATA . . .");
		// printf("=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
		pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
		return PCS_R_CONF_PARSE_ERROR;
	}
	else
	{
		std::string operation_identifier = jsonValue->value.GetString();
		data->job_type = get_job_id_from_frame(operation_identifier);

		if (data->job_type == JOB_UPDATE_CONF)
		{
			jsonValue = jsonDOM.FindMember("data");
			if (jsonValue == jsonDOM.MemberEnd() || !jsonValue->value.IsObject())
			{
				OsLog(LOG_ERROR, "ERROR CONFIGURATION FILE INVALID DATA . . .");
				OsLog(LOG_ERROR, "=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
				pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
				return PCS_R_CONF_PARSE_ERROR;
			}
			else
			{
				rapidjson::StringBuffer buffer;
				rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
				jsonValue->value.Accept(writer);
				unsigned short todo;
				parse_conf(buffer.GetString(), &todo);
				data->polling_interval = todo;
				pcs_operation_send_feedback(posID, data->uuid, EXECUTED_WITH_SUCCESS);
				return PCS_R_OK;
			}
		}
		else if (data->job_type == JOB_ERROR)
		{
			OsLog(LOG_ERROR,"ERROR JOB TYPE DOESN'T EXIST . . .");
			OsLog(LOG_INFO,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
			pcs_operation_send_feedback(posID, data->uuid, ERROR_EXECUTING);
			return PCS_R_CONF_PARSE_ERROR;
		}

		OsLog(LOG_INFO, "JOB TYPE PCS: %s -> RETURNED %d", string(operation_identifier).c_str(), data->job_type);
	}

	pcs_operation_send_feedback(posID, data->uuid, AWAITING_EXECUTION);
	OsLog(LOG_INFO,"=============	MSYS_PCS_GET_LAST_JOB RETURNED	=============\n\n\n\n");
	return PCS_R_OK;
}

int msys_pcs_send_file(const char posID[8], const char *file_content, unsigned short size_content)
{
	OsLog(LOG_INFO, "#############	MSYS_PCS_SEND_FILES CALLED	#############");
	rapidjson::StringBuffer strBuf;
	rapidjson::Writer<rapidjson::StringBuffer> serializer;
	std::string posIdMorph = string(posID, 8);
	strBuf.Clear();
	serializer.Reset(strBuf);
	serializer.StartObject();
	serializer.Key("receiptData");
	serializer.String("");
	serializer.Key("totalizer");
	serializer.String(string(file_content, size_content).c_str());
	serializer.EndObject();
	OsLog(LOG_INFO, "%s", strBuf.GetString());

	stringstream serv;
	serv << "/api/v1/paymentTerminals/" << posIdMorph << "/totalizer";

	if ((curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYPEER, 0) != CURLE_OK) || (curl_easy_setopt(connection->curlHandle, CURLOPT_SSL_VERIFYHOST, 0) != CURLE_OK))
	{
		OsLog(LOG_ERROR,"COULDN'T SETUP SECURE CONNECTION !");
		OsLog(LOG_ERROR, "=============	MSYS_PCS_TEST_COMM RETURNED	=============\n\n\n\n");
		return PCS_R_COMM_ERROR;
	}
	RestClient::Response response = connection->put(serv.str(), strBuf.GetString());

	OsLog(LOG_INFO, "## HTTP CODE : %d", response.code);

	if (response.code != 200)
	{
		if (response.code == 201)
		{
			OsLog(LOG_INFO, "TOTALIZER SUBMITTED SUCCESSFULLY !");
			OsLog(LOG_INFO, "SEND FILE RESPONSE: %s", !response.body.empty() ? string(response.body).c_str() : "NOBODYRESPONSE");
			return PCS_R_OK;
		}
		else if (response.code == 204)
		{
			OsLog(LOG_INFO, "ASSUMING THAT... ALREADY EXISTS?");
			return PCS_R_OK;
		}
		else if (response.code == 28)
		{
			OsLog(LOG_ERROR, "CONNECTION TIMED OUT !");
			OsLog(LOG_ERROR, "=============	MSYS_PCS_SEND_FILES RETURNED	=============\n\n\n\n");
			return PCS_R_TIMEOUT;
		}
		else if (response.code >= 400 || response.code < 0)
		{
			OsLog(LOG_ERROR, "PROBLEM WITH CONNECTION   ::   'LIBCURL' ERROR CODE #%d  ", response.code);
			if (!response.body.empty())
			{
				OsLog(LOG_ERROR, "BODY >> %s", response.body.c_str());
			}
			return PCS_R_COMM_ERROR;
		}
	}

	OsLog(LOG_INFO, "SEND FILE RESPONSE: %s", string(response.body).c_str());
	OsLog(LOG_INFO, "=============	MSYS_PCS_SEND_FILES RETURNED	=============\n\n\n\n");
	return PCS_R_OK;
}
