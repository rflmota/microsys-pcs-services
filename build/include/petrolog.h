#ifndef __PETROLOG__
#define __PETROLOG__

#ifdef __cplusplus
extern "C" {
#endif
#include <stdio.h>
#define PETROLOG_VERSION   "1.2"
/* Enum affinities definition */
#define PETROLOG_LEVELS(X) \
   X(PETROLOG_OFF,      "OFF") \
   X(PETROLOG_FATAL,    "FAT") \
   X(PETROLOG_ERROR,    "ERR") \
   X(PETROLOG_WARN,     "WRN") \
   X(PETROLOG_INFO,     "INF") \
   X(PETROLOG_DEBUG,    "DBG") \
   X(PETROLOG_ALL,      "ALL") \
   X(PETROLOG_VERBOSE,  "VRB") 
                        
/* Enum E_STATUS_ICON_TYPE definition */   
#define PX(a,b) a,
typedef enum { 
   PETROLOG_LEVELS(PX) 
   MAX_LOG_LEVEL 
} PETROLOG_LEVEL;
#undef PX

#define LOGINIT_OK         0
#define LOGINIT_ERROR      1


int petrolog_set_log_file_nr_rolls( int nr_rolls );
int petrolog_set_log_file_size    ( int size );
int petrolog_set_log_filename     ( const char *filename );
int petrolog_set_log_level(PETROLOG_LEVEL level);
int petrolog_trace( PETROLOG_LEVEL level, int no_prefix, const char *function_name, const char *str, ... );
int petrolog_init ( PETROLOG_LEVEL level);
FILE* petrolog_get_fp(void);
void petrolog_close_fp(FILE* fp);
int petrolog_stop ( void );


#define __PTRACE_FAT__(...)                  petrolog_trace( PETROLOG_FATAL,   0, __func__, __VA_ARGS__)
#define __PTRACE_ERR__(...)                  petrolog_trace( PETROLOG_ERROR,   0, __func__, __VA_ARGS__)
#define __PTRACE_WRN__(...)                  petrolog_trace( PETROLOG_WARN,    0, __func__, __VA_ARGS__)
#define __PTRACE_INF__(...)                  petrolog_trace( PETROLOG_INFO,    0, __func__, __VA_ARGS__)
#define __PTRACE_DBG__(...)                  petrolog_trace( PETROLOG_DEBUG,   0, __func__, __VA_ARGS__)
#define __PTRACE_VRB__(...)                  petrolog_trace( PETROLOG_VERBOSE, 0, __func__, __VA_ARGS__)
#define __DEBUG__PF__(...)                   petrolog_trace( PETROLOG_ALL,     0, __func__, __VA_ARGS__)
#define __PTRACE_INF_I__(...)                petrolog_trace( PETROLOG_INFO,    1, __func__, __VA_ARGS__)
#define __PTRACE_HEX__(_A_, _B_, _C_, _D_)   petrolog_trace_hex( _A_, __func__, _B_, _C_, _D_)

#define PetroInitLog(__X__, __Y__, __Z__)    petrolog_init(__X__); petrolog_set_log_file_nr_rolls(__Y__); petrolog_set_log_filename(__Z__);
#define PetroLogClose petrolog_stop
#define PetroLogGetFileFp petrolog_get_fp          
#define PetroLogCloseFileFp petrolog_close_fp
#define PetroLogHex  __PTRACE_HEX__
//#define PetroLog( __X__, __Y__) __PTRACE_ERR__(__Y__); 
#ifdef __cplusplus
}
#endif

#endif


