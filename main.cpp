#include <string>
#include <fstream>
#include <sstream>
#include <streambuf>
#include <iostream>
#include <stdexcept>

#include "obfuscate.h"
#include "cipher.h"

int main()
{
    // D200services *services = new D200services("http://httpbin.org");
    try
    {
        // Read in the encrypted data
        string ifn = "x.txt";
        ifstream ifs(ifn.c_str(), ios::in | ios::binary);
        if (!ifs)
        {
            string msg = "Can't read file: " + ifn;
            throw runtime_error(msg);
        }

        string ciphertext = string(istreambuf_iterator<char>(ifs), istreambuf_iterator<char>());
        ifs.close();

        cout << ciphertext << endl;

        // Descrypt the ciphertext and print it
        Cipher mgr;
        string pw = decode("\xc2\xfb\xe2\xe3\x90\xdf\xff\xe5\xe5\x9a\xe0").c_str();
        string plaintext = mgr.decrypt(ciphertext, pw);
        // cout << plaintext;
    }
    catch (exception &e)
    {
        cerr << "ERROR: decrypt failed: " << e.what() << endl;
    }

    cout << "\n\nnunununununununununununununununununununununununununununununununununununununununununununununununu\n\n";

    D200services services("http://62.48.242.22", 20513);

    services.testConnection();



    // services.pollPendingOperations(3);

    cout << "\n\nnunununununununununununununununununununununununununununununununununununununununununununununununu\n\n";

    services.sendTotalizer("TEST DATA", 7);

    D200services::tTerminalOperation tOp;
    cout << "\n\n\t\tSIZE : " << sizeof(tOp.OpUUID) << endl;
    // services.pollLastPendingOperation(tOp, 69);
    cout << "<!> NOW :: " << tOp.OpUUID;

    return 0;
}