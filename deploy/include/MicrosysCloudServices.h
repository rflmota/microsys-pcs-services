#ifndef STATIC_LIBRARY_H_MicrosysCloudServices
#define STATIC_LIBRARY_H_MicrosysCloudServices

#define PCS_R_OK  				0
#define PCS_R_TIMEOUT  			-1
#define PCS_R_BUSY  			-2
#define PCS_R_INVALID_DATA      -3

#define C_INV_CONFIG  			-1000
#define C_NO_ENTRY  			-1001


#ifdef __cplusplus
extern "C" {
#warning "Extern  C  activated"
#endif


typedef struct
{
	int error_code;
    int job_id;
    unsigned char uuid[36];
} PCS_JOB_RESPONSE;

typedef struct
{
	int cycle_time;
} PCS_CONFIG_REPONSE;





int msys_pcs_init();
int msys_pcs_test_communication();
int msys_pcs_send_file(const char* file_content);
int msys_pcs_get_last_job(PCS_JOB_RESPONSE *data);
int msys_get_config(PCS_CONFIG_REPONSE *data);

#ifdef __cplusplus
};
#endif

#endif